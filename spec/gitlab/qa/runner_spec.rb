describe Gitlab::QA::Runner do
  let(:scenario) { spy('scenario') }
  let(:scenario_arg) { ['Test::Instance::Image'] }

  before do
    stub_const('Gitlab::QA::Scenario', scenario)
  end

  describe '.run' do
    shared_examples 'passes args to scenario' do
      it 'accepts defined options' do
        described_class.run(scenario_arg + passed_args)

        expect(scenario).to have_received(:perform).with(*passed_args)
      end
    end

    context 'when passing gitlab-qa and rspec args' do
      it_behaves_like 'passes args to scenario' do
        let(:passed_args) { %w[CE -- --tag smoke] }
      end
    end

    context 'when enabling a feature flag' do
      it_behaves_like 'passes args to scenario' do
        let(:passed_args) { %w[CE --enable-feature gitaly_enforce_requests_limits] }
      end
    end

    context 'when enabling a feature flag with scenarios with no release specified' do
      it_behaves_like 'passes args to scenario' do
        let(:passed_args) { %w[--enable-feature gitaly_enforce_requests_limits] }
      end
    end

    context 'when specifying an address' do
      it_behaves_like 'passes args to scenario' do
        let(:passed_args) { %w[CE --address http://testurl] }
      end
    end

    context 'when specifying a mattermost server address' do
      it_behaves_like 'passes args to scenario' do
        let(:passed_args) { %w[CE --mattermost-address http://mattermost-server] }
      end
    end

    it 'runs a scenario' do
      described_class.run(scenario_arg)

      expect(scenario).to have_received(:const_get).with('Test::Instance::Image')
    end

    it 'ignores unsupported options' do
      passed_args = %w[CE --foo]

      expect { described_class.run(scenario_arg + passed_args) }.not_to raise_error
    end

    it 'does not pass options to the tests if they are only for GitLab QA' do
      described_class.run(scenario_arg + %w[EE --no-teardown --no-tests])

      expect(scenario).to have_received(:perform).with('EE')
    end
  end
end
