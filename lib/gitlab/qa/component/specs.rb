require 'securerandom'

module Gitlab
  module QA
    module Component
      ##
      # This class represents GitLab QA specs image that is implemented in
      # the `qa/` directory located in GitLab CE / EE repositories.
      #
      class Specs < Scenario::Template
        attr_accessor :suite, :release, :network, :args, :volumes, :env, :runner_network

        TRUSTED_CERTIFICATES_PATH = File.expand_path('../../../../tls_certificates/gitaly/trusted-certs'.freeze, __dir__)
        TRUSTED_PATH = '/etc/ssl/certs'.freeze

        def initialize
          @docker = Docker::Engine.new
          @volumes = {}
          @env = {}
        end

        def perform # rubocop:disable Metrics/AbcSize
          return puts "Skipping tests." if skip_tests?

          raise ArgumentError unless [suite, release].all?

          @docker.login(**release.login_params) if release.login_params

          @docker.pull(release.qa_image, release.qa_tag) unless Runtime::Env.skip_pull?

          puts "Running test suite `#{suite}` for #{release.project_name}"

          name = "#{release.project_name}-qa-#{SecureRandom.hex(4)}"

          feature_flag_sets = []

          # When `args` includes `[..., "--disable-feature", "a", "--enable-feature", "b", ...]`
          # `feature_flag_sets` will be set to `[["--disable-feature", "a"], ["--enable-feature", "b"]]`
          # This will result in tests running twice, once with each feature.
          while (index = args&.index { |x| x =~ /--.*-feature/ })
            feature_flag_sets << args.slice!(index, 2)
          end

          # When `args` do not have either "--disable-feature" or "--enable-feature", we
          # add [] so that test is run exactly once.
          feature_flag_sets << [] unless feature_flag_sets.any?

          feature_flag_sets.each do |feature_flag_set|
            @docker.run(release.qa_image, release.qa_tag, suite, *args_with_flags(args, feature_flag_set)) do |command|
              command << "-t --rm --net=#{network || 'bridge'}"

              env.merge(Runtime::Env.variables).each do |key, value|
                command.env(key, value)
              end

              command.volume('/var/run/docker.sock', '/var/run/docker.sock')
              command.volume(File.join(Runtime::Env.host_artifacts_dir, name), File.join(Docker::Volumes::QA_CONTAINER_WORKDIR, 'tmp'))

              @volumes.to_h.each do |to, from|
                command.volume(to, from)
              end

              command.name(name)
            end
          end
        end

        def mtls
          @volumes[TRUSTED_CERTIFICATES_PATH] = TRUSTED_PATH
        end

        private

        def args_with_flags(args, feature_flag_set)
          return args if feature_flag_set.empty?

          puts "Running with feature flag: #{feature_flag_set.join(' ')}"

          args_with_f = args.dup
          args_with_f.insert(1, *feature_flag_set)
        end

        def skip_tests?
          Runtime::Scenario.attributes.include?(:run_tests) && !Runtime::Scenario.run_tests
        end
      end
    end
  end
end
