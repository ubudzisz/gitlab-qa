module Gitlab
  module QA
    module Scenario
      module Test
        module Integration
          class MTLS < Scenario::Template
            def initialize
              @gitlab_name = 'gitlab'
              @gitaly_name = 'gitaly'
              @spec_suite = 'Test::Instance::All'
              @network = 'test'
              @env = {}
              @tag = 'mtls'
            end

            # rubocop:disable Metrics/AbcSize
            def perform(release, *rspec_args)
              Component::Gitlab.perform do |gitaly|
                gitaly.release = QA::Release.new(release)
                gitaly.name = @gitaly_name
                gitaly.network = @network
                gitaly.skip_availability_check = true

                gitaly.set_accept_insecure_certs
                gitaly.omnibus_config = gitaly_omnibus
                gitaly.mtls

                gitaly.instance do
                  Component::Gitlab.perform do |gitlab|
                    gitlab.release = QA::Release.new(release)
                    gitlab.name = @gitlab_name
                    gitlab.network = @network

                    gitlab.set_accept_insecure_certs
                    gitlab.omnibus_config = gitlab_omnibus
                    gitlab.tls = true
                    gitlab.set_trusted_certificates

                    gitlab.instance do
                      puts "Running mTLS specs!"

                      if @tag
                        rspec_args << "--" unless rspec_args.include?('--')
                        rspec_args << "--tag" << @tag
                      end

                      Component::Specs.perform do |specs|
                        specs.suite = @spec_suite
                        specs.release = gitlab.release
                        specs.network = gitlab.network
                        specs.args = [gitlab.address, *rspec_args]
                        specs.env = @env
                        specs.mtls
                      end
                    end
                  end
                end
              end
            end
            # rubocop:enable Metrics/AbcSize

            def gitlab_omnibus
              <<~OMNIBUS
                gitaly['enable'] = false;

                external_url 'https://#{@gitlab_name}.#{@network}';

                gitlab_rails['gitaly_token'] = 'abc123secret';
                gitlab_shell['secret_token'] = 'shellsecret';

                git_data_dirs({
                  'default' => { 'gitaly_address' => 'tls://#{@gitaly_name}.#{@network}:9999' },
                  'storage1' => { 'gitaly_address' => 'tls://#{@gitaly_name}.#{@network}:9999' },
                });
              OMNIBUS
            end

            def gitaly_omnibus
              <<~OMNIBUS
                gitaly['tls_listen_addr'] = '0.0.0.0:9999';
                gitaly['certificate_path'] = '/etc/gitlab/ssl/gitaly.test.crt';
                gitaly['key_path'] = '/etc/gitlab/ssl/gitaly.test.key';

                postgresql['enable'] = false;
                redis['enable'] = false;
                nginx['enable'] = false;
                puma['enable'] = false;
                sidekiq['enable'] = false;
                gitlab_workhorse['enable'] = false;
                grafana['enable'] = false;
                gitlab_exporter['enable'] = false;
                alertmanager['enable'] = false;
                prometheus['enable'] = false;

                gitlab_rails['rake_cache_clear'] = false;
                gitlab_rails['auto_migrate'] = false;

                gitaly['auth_token'] = 'abc123secret';
                gitlab_shell['secret_token'] = 'shellsecret';

                gitlab_rails['internal_api_url'] = 'https://#{@gitlab_name}.#{@network}';

                git_data_dirs({
                  'default' => { 'path' => '/var/opt/gitlab/git-data' },
                  'storage1' => { 'path' => '/mnt/gitlab/git-data' },
                })
              OMNIBUS
            end
          end
        end
      end
    end
  end
end
